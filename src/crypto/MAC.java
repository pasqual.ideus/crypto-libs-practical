package crypto;

import java.io.IOException;
import java.security.GeneralSecurityException;

public class MAC {

	private String plaintext = "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.";
	private byte[] tag;

	public MAC() {
		try {
			// TODO: Initializing Tink
		} catch (GeneralSecurityException ex) {
			ex.printStackTrace();
		}
	}

	public byte[] sign() throws IOException, GeneralSecurityException {

		try {
			String keysetFilename = "MAC_KEY";
			// TODO Generate the key material. Save the Key. Get primitive and compute the
			// tag.

		} catch (GeneralSecurityException ex) {
			ex.printStackTrace();
		}
		return tag;
	}

	public byte[] getTag() {
		return tag;
	}

	public void setTag(byte[] tag) {
		this.tag = tag;
	}

	public String getPlaintext() {
		return plaintext;
	}

	public void setPlaintext(String plaintext) {
		this.plaintext = plaintext;
	}

	public static void main(String[] args) throws GeneralSecurityException, IOException {
		// MAC m = new MAC();
		// MAC_Verifier mV = new MAC_Verifier();
	}

}