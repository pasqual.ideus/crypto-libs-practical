package Tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.security.GeneralSecurityException;

import org.junit.Test;

import crypto.Digital_Signature_Signer;
import crypto.Digital_Signature_Verifier;

public class Digital_Signature_Test {
	@Test
	public void test() throws IOException, GeneralSecurityException {
		Digital_Signature_Signer dss=new Digital_Signature_Signer();
		Digital_Signature_Verifier dsv= new Digital_Signature_Verifier();
		byte[] signature= dss.signature();
		assertTrue(!signature.equals(null));  
		String plaintext=dss.getPlaintext();
		String toTest=dsv.verify(signature, plaintext);
		assertEquals("Invalid Signature","Valid Signature",toTest);
	
	
	}

}
