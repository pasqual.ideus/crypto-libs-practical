package Tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

import java.io.IOException;
import java.security.GeneralSecurityException;

import org.junit.Test;

import crypto.MAC;
import crypto.MAC_Verifier;

public class MAC_Test {

	String plaintext = "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.";

	@Test
	public void test_sign_notNull() throws IOException, GeneralSecurityException {
		MAC mac = new MAC();
		assertNotNull(mac.sign());
	}

	@Test
	public void test_verify() throws Exception {
		MAC mac = new MAC();
		MAC_Verifier mV = new MAC_Verifier();
		mac.sign();
		assertEquals("verification invalid", "verification valid", mV.verify(mac.getTag(), mac.getPlaintext()));
	}

}
