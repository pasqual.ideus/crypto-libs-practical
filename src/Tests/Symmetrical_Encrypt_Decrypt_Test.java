package Tests;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import crypto.Symmetrical_Encrypt_Decrypt;

public class Symmetrical_Encrypt_Decrypt_Test {
	// Lorem Ipsum Plaintext
	private String expectedPlaintext="Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.";
	
	// Empty Space Plaintext
	private String expectedPlaintext2="          ";
	
	// Emtpy Plaintext
	private String expectedPlaintext3="";

	@Test
	public void testEncryption1() {
		Symmetrical_Encrypt_Decrypt sed=new Symmetrical_Encrypt_Decrypt();
		assertEquals(expectedPlaintext,sed.all(expectedPlaintext));
	
		
	}
	@Test
	public void testEncryption2() {
		Symmetrical_Encrypt_Decrypt sed=new Symmetrical_Encrypt_Decrypt();
		assertEquals(expectedPlaintext2,sed.all(expectedPlaintext2));
	
		
	}
	@Test
	public void testEncryption3() {
		Symmetrical_Encrypt_Decrypt sed=new Symmetrical_Encrypt_Decrypt();
		assertEquals(expectedPlaintext3,sed.all(expectedPlaintext3));
	
		
	}
}
