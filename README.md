# Crypto libs practical

Praktikums Aufgabe zum Thema Crypto Libs aus Softwaresicherheit WS19/20.<br>
Dieses Repository enthält das Java Grundgerüst für das Aufgabenblatt.<br>
Alle weiteren Aufgaben können dort entnommen werden.
## Getting started
<b>Clone the repo to your local machine:</b><br>
Via git-cli:<br>
<code>git clone https://gitlab.com/pasqual.ideus/crypto-libs-practical.git </code><br>
<b>Or copy the following and import it directly into your Eclipse IDE:</b><br>
<code>https://gitlab.com/pasqual.ideus/crypto-libs-practical.git </code><br>

